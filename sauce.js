const { remote } = require('webdriverio');

console.log("edit")

const capabilities = {
 platformName: 'Android',
 'appium:app': 'storage:filename=app-debug.apk',
 'appium:deviceName': 'Android GoogleAPI Emulator',
 'appium:platformVersion': '12.0',
 'appium:automationName': 'UiAutomator2',
 'sauce:options': {
    build: 'appium-build-ID',
    name: 'sauce-client ID',
    deviceOrientation: 'PORTRAIT',
  },
};

async function runTest() {
  const driver = await remote({
    user: process.env.SAUCE_USERNAME,
    key: process.env.SAUCE_ACCESS_KEY,
    hostname: 'ondemand.us-west-1.saucelabs.com',
    port: 443,
    baseUrl: 'wd/hub',
    capabilities,
  });
  let element = await driver.$('//*[@text="Lexi"]')
  await element.click();
}

runTest()
