/// Poll Sauce Labs API for results.

const https = require('node:https');

const user = `${process.env.SAUCE_USERNAME}:${process.env.SAUCE_ACCESS_KEY}`;

const sleep = ms => new Promise(r => setTimeout(r, ms));

function promiseSauceBuilds(ms = 0) {
  sleep(ms);
  return new Promise((resolve, reject) => {
    https.get('https://api.us-west-1.saucelabs.com/v2/builds/vdc/', { auth: user }, (resp) => {
      let body = '';
      resp.on('data', (str) => {
        body += str;
      });
      resp.on('end', () => {
        // Invariant: sorted by creation_time (first is newest)
        resolve(JSON.parse(body).builds[0])
      });
    })
  })
}

async function main() {
  let result = await promiseSauceBuilds();
  while (!result?.end_time) {
    result = await promiseSauceBuilds(15000);
  }

  // const result = { jobs: { passed: 1 }};  // notPassed = false
  // const result = { jobs: { passed: 0 }};  // notPassed = true
  // const result = { };                     // notPassed = true
  const notPassed = result?.jobs?.passed !== 1;

  if (notPassed) {
    process.exitCode = 1;
  }
}
main();
